﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using qrCodeRegistration.Models;

namespace qrCodeRegistration
{
    public class AppDBContext : IdentityDbContext<IdentityUser>
    {
        private readonly IMongoDatabase _db;
       
        public static IMongoClient _client;

        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {
            // _settings = settings.Value;
            _db = Connect();
        }
        public IMongoDatabase Connect()
        {
            var client = new MongoClient("mongodb+srv://dbdev:8yWRKkJuhx3bngxx@version3-mj3ao.mongodb.net/test");
            //var client = new MongoClient("mongodb://localhost:27017/kmbi");
            var database = client.GetDatabase("kmbi");

            return database;
        }

        public IMongoCollection<branch> branch => _db.GetCollection<branch>("branch");
        public IMongoCollection<users> user => _db.GetCollection<users>("users");
    }
}
