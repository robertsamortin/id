﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace qrCodeRegistration.Models
{
    public interface iRepo
    {
        List<branch> getAllBranches();
        bool userExist(string empCode);
        void signUpChecklist(users user);

        users login(string email);

        users getInfo(string slug);
    }
}
