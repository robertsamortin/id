﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace qrCodeRegistration.Models
{
    public interface iUserManager
    {
        users Validate(string username);
        void SignIn(HttpContext httpContext, users user, bool isPersistent = false);
        void SignOut(HttpContext httpContext);
        string GetCurrentUserId(HttpContext httpContext);
        users GetCurrentUser(HttpContext httpContext);
        // registration userDetails(HttpContext httpContext);
    }
}
