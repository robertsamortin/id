﻿using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace qrCodeRegistration.Models
{
    public class Repo : AppDBContext, iRepo
    {
        public Repo(DbContextOptions<AppDBContext> options) : base(options)
        {
        }

        public List<branch> getAllBranches()
        {
            var sort = Builders<branch>.Sort.Ascending(e => e.name);
            return branch.Find(Builders<branch>.Filter.Empty).Sort(sort).ToList();
        }

        public void signUpChecklist(users users)
        {
            user.InsertOneAsync(users);
        }

        public bool userExist(string empCode)
        {
            var query = Builders<users>.Filter.Eq(e => e.employeeCode, empCode);
            var users = user.Find(query).FirstOrDefault();
            if (users != null)
            {
                if (users.employeeCode == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        public users login(string email)
        {
            return user.Find(u => u.email == email).FirstOrDefault();
        }

        public users getInfo(string slug)
        {
            return user.Find(u => u.slug == slug).FirstOrDefault();
        }
    }
}
