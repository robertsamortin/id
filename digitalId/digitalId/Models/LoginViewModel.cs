﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace qrCodeRegistration.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "ID Number must not be empty.")]
        [Display(Name = "Email Address")]
        public string email { get; set; }

        [Required(ErrorMessage = "Password must not be empty.")]
        public string password { get; set; }

        public string ReturnUrl { get; set; }
    }
}
