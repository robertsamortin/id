﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace qrCodeRegistration.Models
{
    public class branchViewModel
    {
        public string slug { get; set; }
        public string name { get; set; }
    }
    public class branch
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("regionId")]
        public string regionId { get; set; }

        [BsonElement("areaId")]
        public string areaId { get; set; }

        [BsonElement("coordinates")]
        public coordinates coordinates { get; set; }

        [BsonElement("head")]
        public string head { get; set; }

        [BsonElement("ba")]
        public string ba { get; set; }

        [BsonElement("baa")]
        public string baa { get; set; }

        [BsonElement("isOperational")]
        public int? isOperational { get; set; }

        [BsonElement("tinNumber")]
        public string tinNumber { get; set; }

        [BsonElementAttribute("holidays")]
        public List<holiday> holidays { get; set; }

        [BsonElement("units")]
        public IList<units> units { get; set; }

        [BsonElement("banks")]
        public IList<banks> banks { get; set; }

        [BsonElement("province")]
        public branchProvince province { get; set; }

        [BsonElement("paisId")]
        public String paisId { get; set; }

        [BsonElement("paisLastSyncAt")]
        public DateTime? paisLastSyncAt { get; set; }

        [BsonElement("lastSyncAt")]
        public DateTime lastSyncAt { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class coordinates
    {
        [BsonElement("latitude")]
        public double latitude { get; set; }

        [BsonElement("longitude")]
        public double longitude { get; set; }
    }

    public class branchProvince
    {
        public string slug { get; set; }
        public string regionSlug { get; set; }
        public string name { get; set; }
    }

    public class banks
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class units
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("head")]
        public string head { get; set; }

        //[BsonElement("officers")]
        //public IList<officers> officers { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class holiday
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
        [BsonElement("name")]
        public String name { get; set; }
        [BsonElement("slug")]
        public String slug { get; set; }
        [BsonElementAttribute("date")]
        public DateTime date { get; set; }
        [BsonElement("type")]
        public String type { get; set; }
        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }
        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }
}
