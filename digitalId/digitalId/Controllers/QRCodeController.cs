﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using digitalId.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QRCoder;
using qrCodeRegistration.Models;

namespace qrCodeRegistration.Controllers
{
    [Authorize]
    public class QRCodeController : Controller
    {
        private iUserManager _userManager;
        private iRepo _repo;

        public QRCodeController(iUserManager userManager, iRepo repo)
        {
            _userManager = userManager;
            _repo = repo;
        }
        public IActionResult Index()
        {
            var picturePath = _userManager.GetCurrentUser(this.HttpContext).picture.ToString();
            var filename = Path.GetFileName(picturePath);
            //var picImg = File(picturePath, "image/png", filename);

            //var destinationPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images/") + Path.GetFileName(picturePath) ;
            //System.IO.File.Copy(picturePath, destinationPath);
            byte[] imageBytes = new byte[] { };

            using (WebClient webClient = new WebClient())
            {
                byte[] data = webClient.DownloadData(picturePath);

                using (MemoryStream mem = new MemoryStream(data))
                {
                    using (var yourImage = Image.FromStream(mem))
                    {
                        // If you want it as Png
                        yourImage.Save(filename, ImageFormat.Png);
                        imageBytes = mem.ToArray();
                    }
                }

            }

            

            //using (Image image = Image.FromFile(destinationPath))
            //{
            //    using (MemoryStream m = new MemoryStream())
            //    {
            //        image.Save(m, image.RawFormat);
            //        imageBytes = m.ToArray();

            //        // Convert byte[] to Base64 String
            //        string base64String = Convert.ToBase64String(imageBytes);
            //        // return base64String;
            //    }
            //}

            string currURL = $"{this.Request.Scheme}://{this.Request.Host}{this.Request.PathBase}";
            string slug = _userManager.GetCurrentUser(this.HttpContext).slug.ToString();
            var link = currURL + "/Home/Details?slug=" + slug;
            ViewBag.Slug = link;
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(link, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            mModel viewModel = new mModel();
            viewModel.empPict = imageBytes;
            viewModel.qrCode = BitmapToBytes(qrCodeImage);

            //return View(BitmapToBytes(qrCodeImage));
            return View(viewModel);
        }

        private static Byte[] BitmapToBytes(Bitmap img)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        public IActionResult Download()
        {
            string slug = _userManager.GetCurrentUser(this.HttpContext).slug.ToString();

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(slug, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            return File(BitmapToBytes(qrCodeImage), "image/png", "download.png");
        }

        
    }

   
    
}