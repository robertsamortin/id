﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using qrCodeRegistration.Models;

namespace qrCodeRegistration.Controllers
{
    public class AccountController : Controller
    {
        iRepo _repo;
        iUserManager _userManager;

        public AccountController(iRepo repo, iUserManager userManager)
        {
            _repo = repo;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
           
            return View();
        }

        public IActionResult SignUp()
        {
            //ViewData["Message"] = "Your application description page.";
            ViewBag.ListBranches = getBranches();
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public List<branchViewModel> getBranches()
        {
            List<branchViewModel> m = new List<branchViewModel>();
            var c = _repo.getAllBranches();
            foreach (var item in c)
            {
                m.Add(new branchViewModel
                {
                    slug = item.slug,
                    name = item.name
                });
            }
            m.Insert(0, new branchViewModel { slug = "", name = "Select Branch" });
            return m;
        }

        [HttpGetAttribute]
        public bool CheckUserIfExist(string empCode)
        {
            return _repo.userExist(empCode);
        }

        [HttpPost]
        public IActionResult signUpChecklists([Bind] usersViewModel viewModel)
        {
            users users = new users {
                password = BCrypt.Net.BCrypt.HashPassword("qweadszxc"),
                token = encryptDecrypt.EncryptString(viewModel.firstName.ToUpper().Substring(0, 1) + viewModel.middleName.ToUpper().Substring(0, 1) + viewModel.lastName.ToUpper()),
                verificationToken = encryptDecrypt.EncryptString(viewModel.firstName.ToUpper().Substring(0, 1) + viewModel.middleName.ToUpper().Substring(0, 1) + viewModel.lastName.ToUpper()),
                email = viewModel.firstName.Substring(0, 1).ToLower() + viewModel.middleName.Substring(0, 1).ToLower() +
                viewModel.lastName.ToLower().Replace(" ", string.Empty) + "@kmbi.org.ph",
                employeeCode = viewModel.id,
                isActive = 1,
                isVerified = 0,
                permissions = new List<string> (
                    new string[]{ "dashboard-redirect-profile" }
                ),
                name = new Name {
                    first = viewModel.firstName.ToUpper(),
                    middle = viewModel.middleName.ToUpper(),
                    last = viewModel.lastName.ToUpper()
                },
                fullname = viewModel.firstName.ToUpper() + " " + viewModel.middleName.ToUpper() + " " + viewModel.lastName.ToUpper(),
                gender = viewModel.gender.ToUpper(),
                birthdate = viewModel.birthdate.Date,
                addresses = new List<address> {
                    new address{
                        slug = "presentaddress",
                        type = "Present Address",
                        location = viewModel.address,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now
                    },
                    new address{
                        slug = "permanentaddress",
                        type = "Permanent Address",
                        location = viewModel.address,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now
                    }
                },
                employment = new employment {
                    position = viewModel.position.ToUpper(),
                    branch = viewModel.branch,
                    salary = encryptDecrypt.EncryptString("0.00")
                },
                contacts = new List<contact> {
                    new contact{
                        mobile = viewModel.contact,
                        landline = viewModel.contact,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now
                    }
                },
                createdAt = DateTime.Now,
                updatedAt = DateTime.Now
            };
            
           _repo.signUpChecklist(users);
            return RedirectToAction("SignIn");
        }

       

        public IActionResult SignIn(string returnUrl)
        {
            return View(new LoginViewModel
            {
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public IActionResult SignIn(LoginViewModel lv, string returnUrl = null)
        {
            this.ViewData["ReturnUrl"] = returnUrl;

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("Error", "User name/password not found");
                return View(lv);
            }

            var user = _userManager.Validate(lv.email);

            if (user != null)
            {
                var model = _repo.login(lv.email);
                var hashedPassword = model.password;
                bool validPassword = BCrypt.Net.BCrypt.Verify(lv.password, hashedPassword);

                if (validPassword)
                {
                    _userManager.SignIn(this.HttpContext, user, false);

                    var d = User.Identity.IsAuthenticated;
                   

                    if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                        return Redirect(returnUrl);
                    else
                        return RedirectToAction("Index", "QRCode");
                }
            }

            ModelState.AddModelError("Error", "User name/password not found");
            return View(lv);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Logout()
        {
            _userManager.SignOut(this.HttpContext);
            return this.RedirectToAction("SignIn", "Account");
        }
    }


}
